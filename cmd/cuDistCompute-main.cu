#include <iostream>
#include <fstream>
#include <chrono>
#include <ostream>
#include <vector>
#include <cmath>

// -- gpu computation --
#include "cuDistCompute/gpu_compute.cuh"

std::string projDirectory = "/gpfs/data/bkimia/cchien3/master-students-proj/magma-gpu-vecdistcompute/";

int main(int argc, char **argv) {

  std::cout << std::endl;

  // -- User-defined parameters --
  std::string filename_X = projDirectory;
  std::string filename_Y = projDirectory;
  filename_X = filename_X.append("target_data/target_data_X.txt");
  filename_Y = filename_Y.append("target_data/target_data_Y.txt");

  // -- gpu work flow --
  //bool success_read = cuDistComputeWrapper::gpu_compute_dim_per_thd(filename_X, filename_Y);
  bool success_read = cuDistComputeWrapper::gpu_compute_dist_per_thd(filename_X, filename_Y);
  if (!success_read) exit(1);

  return 0;
}
