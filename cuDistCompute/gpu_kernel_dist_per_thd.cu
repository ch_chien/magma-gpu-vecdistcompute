#ifndef gpu_kernels_cu
#define gpu_kernels_cu

#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_compute.cuh"

namespace cuDistComputeWrapper {

  template<int dimPerVec>
  __global__ void vecDistCompute(
    magmaFloatComplex** d_target_data_X_array,
    magmaFloatComplex** d_target_data_Y_array, magma_int_t ldd)
  {
    // -- thread id and block id --
    const int tx = threadIdx.x;
    const int bx = blockIdx.x;

    // ===================================================================
    // 1) Memory use declarations
    // ===================================================================
    // -- data arrays --
    magmaFloatComplex* d_data_X = d_target_data_X_array[bx];
    magmaFloatComplex* d_data_Y = d_target_data_Y_array[bx];

    // -- REGISTER: declarations --
    magmaFloatComplex r_data_X[dimPerVec]  = {MAGMA_C_ZERO};
    magmaFloatComplex r_data_Y[dimPerVec]  = {MAGMA_C_ZERO};
    magmaFloatComplex r_data_norm  = MAGMA_C_ZERO;

    // -- SHARED MEMORY: declarations --
    //extern __shared__ magmaFloatComplex zdata[];
    //magmaFloatComplex *s_data_dist = (magmaFloatComplex *)(zdata);

    // -- shared memory array initialization --
    //s_data_dist[tx] = MAGMA_C_ZERO;

    // ===================================================================
    // 2) Load data from GLOBAL MEMORY to REGISTERS
    // ===================================================================
/*    #pragma unroll
    for(int i = 0; i < dimPerVec; i++){
        r_data_X[i] = d_data_X[ i + tx * dimPerVec ];
        r_data_Y[i] = d_data_Y[ i + tx * dimPerVec ];
    }*/
    #pragma unroll
    for(int i = 0; i < dimPerVec; i++){
        r_data_X[i] = d_data_X[ tx + i * dimPerVec ];
        r_data_Y[i] = d_data_Y[ tx + i * dimPerVec ];
    }

    // ===================================================================
    // 3) Do kernel jobs
    // ===================================================================
    #pragma unroll
    for(int i = 0; i < dimPerVec; i++){
	r_data_norm += (r_data_X[i] - r_data_Y[i]) * (r_data_X[i] - r_data_Y[i]);
    }
    //__syncthreads();

    // ===================================================================
    // 4) Load data from SHARED MEMORY to GLOBAL MEMORY
    // ===================================================================
    d_data_Y[ tx * dimPerVec ] = r_data_norm;
  }

  void kernel_launcher_dist_per_thd(
    magma_int_t num_of_thd_per_block, magma_int_t num_of_blocks,
    magmaFloatComplex** d_target_data_X_array,
    magmaFloatComplex** d_target_data_Y_array,
    magma_int_t ldd, magma_queue_t my_queue)
  {
    // -- declare number of threads in a block --
    dim3 threads(num_of_thd_per_block, 1, 1);
    // -- declare number of blocks --
    dim3 grid(num_of_blocks, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    // -- declare shared memory size --
    magma_int_t shmem  = 0;
    shmem += num_of_thd_per_block * sizeof(magmaFloatComplex);	// -- distance of each dimension of a vector --

    // -- kernel arguments --
    void *kernel_args[] = {&d_target_data_X_array, &d_target_data_Y_array, &ldd};

    // -- launch the kernel! --
    e = cudaLaunchKernel((void*)vecDistCompute<32>, grid, threads, kernel_args, shmem, my_queue->cuda_stream());
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of vecDistCompute is not successful!\n");
    }
  }
}

#endif
