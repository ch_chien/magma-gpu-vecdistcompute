#ifndef input_info_cpp_
#define input_info_cpp_
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"

#include "input-info.h"

namespace cmdInputs {

  // -- read data file --
  extern "C" bool readFiles_data(
    std::string targetData_filename, magmaFloatComplex *h_targetData)
  {
    std::string targetData_dir = targetData_filename;
    std::fstream targetData_file;

    float s_real, s_imag;
    int d = 0;

    std::cout<<targetData_dir<<std::endl;

    // -- read start system coefficients --
    targetData_file.open(targetData_dir, std::ios_base::in);
    if (!targetData_file) {
      std::cerr << "target data file not existed!\n";
      return 0;
    }
    else {
      while (targetData_file >> s_real >> s_imag) {
        // -- store the complex number in h_targetData --
        (h_targetData)[d] = MAGMA_C_MAKE(0.001, 0.0) * MAGMA_C_MAKE(s_real, s_imag);
        d++;
      }

      return 1;
    }
  }
}

#endif
