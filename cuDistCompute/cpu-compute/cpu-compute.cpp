#ifndef cpu_compute_cpp_
#define cpu_compute_cpp_
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"

#include "cpu-compute.h"

namespace cuDistComputeWrapper {

  // -- read data file --
  extern "C" void cpu_vecDistCompute(
    magma_int_t dim_per_vector, magma_int_t num_of_vectors,
    magmaFloatComplex *h_targetData_X, magmaFloatComplex *h_targetData_Y,
    magmaFloatComplex *h_cpu_vecNorms)
  {
    
    for(int i = 0; i < num_of_vectors; i++) {
        h_cpu_vecNorms[i] = MAGMA_C_ZERO;
	for(int j = 0; j < dim_per_vector; j++) {
	    h_cpu_vecNorms[i] += ((h_targetData_X + i * dim_per_vector)[j] - (h_targetData_Y + i * dim_per_vector)[j]) * ((h_targetData_X + i * dim_per_vector)[j] - (h_targetData_Y + i * dim_per_vector)[j]);
	}
    }
  }

  extern "C" bool check_gpu_cpu_results(
     magma_int_t dim_per_vector, magma_int_t num_of_vectors,
     magmaFloatComplex *h_gpu_vecNorms, magmaFloatComplex *h_cpu_vecNorms) 
  {
    int num_of_correctness = 0;
    float err_real_part;
    float err_imag_part;
    for(int i = 0; i < num_of_vectors; i++) {
	err_real_part = abs(MAGMA_C_REAL((h_gpu_vecNorms + i * dim_per_vector)[0]) - MAGMA_C_REAL(h_cpu_vecNorms[i]));
	err_imag_part = abs(MAGMA_C_IMAG((h_gpu_vecNorms + i * dim_per_vector)[0]) - MAGMA_C_IMAG(h_cpu_vecNorms[i]));
        if ( err_real_part < 1 && err_imag_part < 1 )
		num_of_correctness ++;
    }
    
    if (num_of_correctness == num_of_vectors)
	return 1;
    else
	return 0;
  }
}

#endif
