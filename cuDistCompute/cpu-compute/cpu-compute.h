#ifndef cpu_compute_h_
#define cpu_compute_h_

extern "C" {
  namespace cuDistComputeWrapper {
    
    // -- cpu computation on vector norm --
    void cpu_vecDistCompute(
       magma_int_t dim_per_vector, magma_int_t num_of_vectors,
       magmaFloatComplex *h_targetData_X, magmaFloatComplex *h_targetData_Y,
       magmaFloatComplex *h_cpu_vecNorms
    );

    // -- verify the computation results from GPU and CPU --
    bool check_gpu_cpu_results(
       magma_int_t dim_per_vector, magma_int_t num_of_vectors,
       magmaFloatComplex *h_gpu_vecNorms, magmaFloatComplex *h_cpu_vecNorms
    ); 
  }
}

#endif
