#ifndef input_info_h_
#define input_info_h_

extern "C" {
  namespace cmdInputs {
    bool readFiles_data(std::string targetData_filename, magmaFloatComplex *h_targetData);
  }
}

#endif
