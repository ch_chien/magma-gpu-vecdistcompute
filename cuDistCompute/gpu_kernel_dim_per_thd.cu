#ifndef gpu_kernels_cu
#define gpu_kernels_cu

#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "gpu_compute.cuh"

namespace cuDistComputeWrapper {

  template<int dimPerVec>
  __device__ __inline__ void
  dev_compute_vec_norm(
    const int tx, 
    magmaFloatComplex r_data_X, magmaFloatComplex r_data_Y, 
    magmaFloatComplex *s_data_dist)
  {
    // -- 3-1) compute distance of each dimension --
    s_data_dist[tx] = (r_data_X - r_data_Y) * (r_data_X - r_data_Y);
    __syncthreads();

    // -- 3-2) sum over all distances --
    magma_sum_reduce<dimPerVec>(tx, s_data_dist);
  }

  template<int dimPerVec>
  __global__ void vecDistCompute(
    magmaFloatComplex** d_target_data_X_array, magmaFloatComplex** d_target_data_Y_array,
    magmaFloatComplex** d_target_data_dist_array, magma_int_t ldd)
  {
    // -- thread id and block id --
    const int tx = threadIdx.x;
    const int bx = blockIdx.x;

    // ===================================================================
    // 1) Memory use declarations
    // ===================================================================
    // -- data arrays --
    magmaFloatComplex* d_data_X = d_target_data_X_array[bx];
    magmaFloatComplex* d_data_Y = d_target_data_Y_array[bx];

    // -- REGISTER: declarations --
    magmaFloatComplex r_data_X  = MAGMA_C_ZERO;
    magmaFloatComplex r_data_Y  = MAGMA_C_ZERO;
    magmaFloatComplex r_data_dist  = MAGMA_C_ZERO;

    // -- SHARED MEMORY: declarations --
    extern __shared__ magmaFloatComplex zdata[];
    magmaFloatComplex *s_data_dist = (magmaFloatComplex *)(zdata);

    // ===================================================================
    // 2) Load data from GLOBAL MEMORY to REGISTERS
    // ===================================================================
    r_data_X = d_data_X[ tx ];
    r_data_Y = d_data_Y[ tx ];

    // ===================================================================
    // 3) Do kernel jobs
    // ===================================================================
    //r_data_dist = (r_data_X - r_data_Y) * (r_data_X - r_data_Y);
    dev_compute_vec_norm< dimPerVec >( tx, r_data_X, r_data_Y, s_data_dist);

    // ===================================================================
    // 4) Load data from SHARED MEMORY to GLOBAL MEMORY
    // ===================================================================
    //d_data_Y[ tx ] = r_data_dist;
    d_data_Y[ tx ] = s_data_dist[0];
  }

  void kernel_launcher_dim_per_thd(
    magma_int_t dim_per_vector, magma_int_t num_of_vectors,
    magmaFloatComplex** d_target_data_X_array, magmaFloatComplex** d_target_data_Y_array,
    magmaFloatComplex** d_target_data_dist_array,
    magma_int_t ldd, magma_queue_t my_queue)
  {
    // -- define numbers of threads and blocks --
    const magma_int_t num_of_threads = dim_per_vector;
    const magma_int_t num_of_blocks = num_of_vectors;

    // -- declare number of threads in a block --
    dim3 threads(num_of_threads, 1, 1);
    // -- declare number of blocks --
    dim3 grid(num_of_blocks, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    // -- declare shared memory size --
    magma_int_t shmem  = 0;
    shmem += dim_per_vector * sizeof(magmaFloatComplex);	// -- distance of each dimension of a vector --

    // -- kernel arguments --
    void *kernel_args[] = {&d_target_data_X_array, &d_target_data_Y_array, &d_target_data_dist_array, &ldd};

    // -- launch the kernel! --
    e = cudaLaunchKernel((void*)vecDistCompute<32>, grid, threads, kernel_args, shmem, my_queue->cuda_stream());
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of vecDistCompute is not successful!\n");
    }
  }
}

#endif
