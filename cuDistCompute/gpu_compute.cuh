#ifndef gpu_compute_cuh
#define gpu_compute_cuh

#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "magma_v2.h"
#include "magma_lapack.h"

namespace cuDistComputeWrapper {

  // -- gpu work flow: dimension per thread --
  bool gpu_compute_dim_per_thd(
    std::string filename_1,
    std::string filename_2
  );

  // -- gpu work flow: distance per thread --
  bool gpu_compute_dist_per_thd(
    std::string filename_1,
    std::string filename_2
  );

  // -- gpu kernel work flow: dimension per thread --
  void kernel_launcher_dim_per_thd(
    magma_int_t dim_per_vector, magma_int_t num_of_vectors,
    magmaFloatComplex** d_target_data_X_array, magmaFloatComplex** d_target_data_Y_array,
    magmaFloatComplex** d_target_data_dist_array,
    magma_int_t ldd, magma_queue_t my_queue
  );

  // -- gpu kernel work flow: distance per thread --
  void kernel_launcher_dist_per_thd(
    magma_int_t num_of_thd_per_block, magma_int_t num_of_blocks,
    magmaFloatComplex** d_target_data_X_array, magmaFloatComplex** d_target_data_Y_array,
    magma_int_t ldd, magma_queue_t my_queue
  );

}

#endif
