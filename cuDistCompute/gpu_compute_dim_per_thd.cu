#ifndef gpu_compute_cu
#define gpu_compute_cu

#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- cuda included --
#include <cuda.h>
#include <cuda_runtime.h>

// -- headers --
#include "input-info.h"
#include "gpu_compute.cuh"
#include "cpu-compute/cpu-compute.h"

namespace cuDistComputeWrapper {

  bool gpu_compute_dim_per_thd( std::string filename_1, std::string filename_2 )
  {
    // ===================================================================
    // 1) magma initialization
    // ===================================================================
    magma_init();
    magma_print_environment();
    magma_queue_t my_queue;                    // magma queue variable, internally holds a cuda stream and a cublas handle
    magma_device_t cdev;                       // variable to indicate current gpu id
    magma_getdevice( &cdev );
    magma_queue_create( cdev, &my_queue );     // create a queue on this cdev
    real_Double_t  gpu_time;
    real_Double_t  cpu_time;

    // -- define memory sizes --
    magma_int_t dim_per_vector = 32;           // -- number of data a block computes (dimension of each vector) --
    magma_int_t num_of_vectors = 1152;          // -- number of blocks (number of vectors) --
    magma_int_t ld    = dim_per_vector;
    magma_int_t ldd   = magma_roundup( dim_per_vector, 32 );  // multiple of 32 by default

    // ===================================================================
    // 2) CPU and GPU arrays declarations
    // ===================================================================
    // -- CPU arrays --
    magmaFloatComplex 	   *h_targetData_X;
    magmaFloatComplex 	   *h_targetData_Y;
    magmaFloatComplex	   *h_gpu_vecNorms;
    magmaFloatComplex	   *h_cpu_vecNorms;
    // -- GPU array address pointer --
    magmaFloatComplex_ptr   d_target_data_X;
    magmaFloatComplex_ptr   d_target_data_Y;
    magmaFloatComplex_ptr   d_target_data_dist;
    // -- GPU two-dim arrays --
    magmaFloatComplex     **d_target_data_X_array = NULL;
    magmaFloatComplex     **d_target_data_Y_array = NULL;
    magmaFloatComplex     **d_target_data_dist_array = NULL;

    // ===================================================================
    // 3) allocate CPU and GPU memories
    // ===================================================================
    // -- CPU arrays --
    magma_cmalloc_cpu( &h_targetData_X, dim_per_vector*num_of_vectors );
    magma_cmalloc_cpu( &h_targetData_Y, dim_per_vector*num_of_vectors );
    magma_cmalloc_cpu( &h_gpu_vecNorms, dim_per_vector*num_of_vectors );
    magma_cmalloc_cpu( &h_cpu_vecNorms, num_of_vectors );
    // -- GPU array address pointer --
    magma_cmalloc( &d_target_data_X, ldd*num_of_vectors );
    magma_cmalloc( &d_target_data_Y, ldd*num_of_vectors );
    magma_cmalloc( &d_target_data_dist, ldd*num_of_vectors );
    // -- GPU two-dim arrays --
    magma_malloc( (void**) &d_target_data_X_array,    num_of_vectors * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_target_data_Y_array,    num_of_vectors * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_target_data_dist_array, num_of_vectors * sizeof(magmaFloatComplex*) );

    // -- read complex number from the files --
    bool success_read_X = cmdInputs::readFiles_data(filename_1, h_targetData_X);
    bool success_read_Y = cmdInputs::readFiles_data(filename_2, h_targetData_Y);
    if (!success_read_X || !success_read_Y) {
      std::cout<<"read files failed!"<<std::endl;
      return 0;
    }

    // ===================================================================
    // 3) copy data from CPU memory to GPU memory
    // ===================================================================
    // -- copy CPU array values to GPU arrays, SIZE = dim_per_vector*num_of_vectors --
    magma_csetmatrix( dim_per_vector, num_of_vectors, h_targetData_X, ld, d_target_data_X, ldd, my_queue );
    magma_csetmatrix( dim_per_vector, num_of_vectors, h_targetData_Y, ld, d_target_data_Y, ldd, my_queue );
    // -- set the address pointer, SIZE = dim_per_vector*ldd --
    magma_cset_pointer( d_target_data_X_array, d_target_data_X, ldd, 0, 0, ldd, num_of_vectors, my_queue );
    magma_cset_pointer( d_target_data_Y_array, d_target_data_Y, ldd, 0, 0, ldd, num_of_vectors, my_queue );
    magma_cset_pointer( d_target_data_dist_array, d_target_data_dist, ldd, 0, 0, ldd, num_of_vectors, my_queue );

    // -- print out the target data 1 & 2 --
    int s = 100;
    //std::cout<<"-- data --"<<std::endl;
    //magma_cprint(dim_per_vector, 1, h_targetData_X + s * ld, ld);
    //magma_cprint(dim_per_vector, 1, h_targetData_Y + s * ld, ld);

    // ===================================================================
    // 4) launch the kernel and let each thread of GPU does the kernel job
    // ===================================================================
    std::cout<<"GPU computing ..."<<std::endl;
    gpu_time = magma_sync_wtime( my_queue );
    kernel_launcher_dim_per_thd( dim_per_vector, num_of_vectors, d_target_data_X_array, d_target_data_Y_array, d_target_data_dist_array, ldd, my_queue );
    gpu_time = magma_sync_wtime( my_queue ) - gpu_time;

    // ===================================================================
    // 5) Copy data from GPU memory to CPU memory
    // ===================================================================
    magma_cgetmatrix( dim_per_vector, num_of_vectors, d_target_data_Y, ldd, h_gpu_vecNorms, ld, my_queue );

    // -- print out the GPU computation results --
    //magma_cprint(dim_per_vector, 1, h_gpu_vecNorms + s * ld, ld);

    // ===================================================================
    // 6) Optional: CPU computation
    // ===================================================================
    cpu_time = magma_sync_wtime( my_queue );
    cpu_vecDistCompute( dim_per_vector, num_of_vectors, h_targetData_X, h_targetData_Y, h_cpu_vecNorms);
    cpu_time = magma_sync_wtime( my_queue ) - cpu_time;

    // ===================================================================
    // 7) Optional: Check numerical equivalence between CPU and GPU
    // ===================================================================
    // -- verify whether it is numerically correct for both cpu and gpu --
    bool okay = check_gpu_cpu_results( dim_per_vector, num_of_vectors, h_gpu_vecNorms, h_cpu_vecNorms );

    // ===================================================================
    // 8) Print out the timing results
    // ===================================================================
    printf("%% DataDim   DataSize   CPU time (msec)   GPU time (msec)   Residual\n");
    printf("%%=============================================================================\n");
    std::cout<<"    "<<(long long) dim_per_vector<<"         "<<(long long) num_of_vectors<<"       ";
    std::cout<< cpu_time*1000<<"        "<< gpu_time*1000<<"         "<<(okay ? "ok" : "failed")<<std::endl;

    // ===================================================================
    // 9) Free CPU and GPU memories
    // ===================================================================
    // -- magma --
    magma_queue_destroy( my_queue );
    // -- CPU --
    magma_free_cpu( h_targetData_X );
    magma_free_cpu( h_targetData_Y );
    magma_free_cpu( h_gpu_vecNorms );
    magma_free_cpu( h_cpu_vecNorms );
    // -- GPU --
    magma_free( d_target_data_X );
    magma_free( d_target_data_Y );
    magma_free( d_target_data_dist );
    magma_free( d_target_data_X_array );
    magma_free( d_target_data_Y_array );
    magma_free( d_target_data_dist_array );

    fflush( stdout );
    magma_finalize();

    return 1;
  }
}

#endif
