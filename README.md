# How to run this code on Brown University CCV
# 1. Prerequisites
(1) Make sure to load the required modules
```bash
$ module load cmake/3.15.4
$ module load gcc/10.2
$ module load cuda/11.1.1
$ module load openblas/0.3.7
```
(2) Include the magma's shared library to the environmental variables
``` bash
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/gpfs/data/bkimia/magma-cchien/lib
```
(3) Launch an interact gpu node for x hours
```bash
$ interact -q gpu -t 0x:00:00 -g 1
```
# 2. Change user-defined directories in the repo
(1) cd to your CCV directory and clone the repository (repo)
```bash
$ git clone https://ch_chien@bitbucket.org/ch_chien/magma-gpu-vecdistcompute.git
```
(2) cd to the file where the main function is
```bash
$ cd magma-gpu-vecdistcompute/cmd
```
(3) open the cuDistCompute-main.cu file and change the repo directory in line 11
```bash
$ vim cuDistCompute-main.cu
```
# 3. Compile and run the code
(1) cd to the repo folder, create a 'build' directory and enter that folder
```bash
$ mkdir build && cd build
```
(2) under the build folder, create a make file
```bash
$ cmake ..
```
(3) compile the entire code
```bash
$ make -j
```
(4) under the build folder, cd to the bin folder
```bash
$ cd bin
```
(5) run the code by execute the execution file
```bash
$ ./cuDistCompute-main
```
